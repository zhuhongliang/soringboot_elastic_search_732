package com.zhl.elasticsearch_demo;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.zhl.elasticsearch_demo.pojo.Article;
import com.zhl.elasticsearch_demo.pojo.Article2;
import com.zhl.elasticsearch_demo.service.impl.ServiceImpl;
import org.elasticsearch.search.SearchHit;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ElasticsearchDemoApplicationTests {

    Logger log = LoggerFactory.getLogger(this.getClass());
    @Autowired
    ServiceImpl commodityServiceImpl;


    /**
     * 索引是否存在
     */
    @Test
    public void indexExist(){
        commodityServiceImpl.exist(Article.class.getSimpleName());
    }



    /**
     * 创建索引
     */
    @Test
    public void indexCreate(){
        Boolean exist = commodityServiceImpl.exist(Article2.class.getSimpleName());
        if(!exist){
            log.info("需要创建新的索引");
            Boolean result = commodityServiceImpl.createIndex2(Article2.class);
            log.info("创建索引结果：{}",result);
            return;
        }
        log.info("索引已存在，不需要创建新的");
    }

    /**
     * 删除索引
     */
    @Test
    public void indexDelete(){
        commodityServiceImpl.delete(Article.class.getSimpleName());
    }

    /**
     * 测试添加文档
     */
    @Test
    public void createDoc(){
        Article article = new Article();
        article.setTitle("水调歌头·明月几时有");
        article.setContent("丙辰中秋，欢饮达旦，大醉，作此篇，兼怀子由。\n" +
                "\n" +
                "明月几时有？把酒问青天。不知天上宫阙，今夕是何年。我欲乘风归去，又恐琼楼玉宇，高处不胜寒。起舞弄清影，何似在人间？(何似 一作：何时；又恐 一作：惟 / 唯恐)\n" +
                "转朱阁，低绮户，照无眠。不应有恨，何事长向别时圆？人有悲欢离合，月有阴晴圆缺，此事古难全。但愿人长久，千里共婵娟。(长向 一作：偏向)");
        article.setCreate_time(new Date());
        String result = commodityServiceImpl.createDoc(article);
        log.info("添加文档返回的id：{}",result);
    }

    /**
     * 测试批量添加文档
     */
    @Test
    public void createMultiDoc(){


        Article article1 = new Article();
        article1.setTitle("声声慢·寻寻觅觅");
        article1.setContent("寻寻觅觅，冷冷清清，凄凄惨惨戚戚。乍暖还寒时候，最难将息。三杯两盏淡酒，怎敌他、晚来风急？雁过也，正伤心，却是旧时相识。\n" +
                "满地黄花堆积，憔悴损，如今有谁堪摘？守着窗儿，独自怎生得黑？梧桐更兼细雨，到黄昏、点点滴滴。这次第，怎一个愁字了得！（守着窗儿 一作：守著窗儿）测试春晚");
        article1.setCreate_time(new Date());

        Article article2 = new Article();
        article2.setTitle("卜算子·咏梅");
        article2.setContent("驿外断桥边，寂寞开无主。已是黄昏独自愁，更著风和雨。(著 同：着) \n" +
                "无意苦争春，一任群芳妒。零落成泥碾作尘，只有香如故。");
        article2.setCreate_time(new Date());

        Article article3 = new Article();
        article3.setTitle("武陵春·春晚");
        article3.setContent("风住尘香花已尽，日晚倦梳头。物是人非事事休，欲语泪先流。\n" +
                "闻说双溪春尚好，也拟泛轻舟。只恐双溪舴艋舟，载不动许多愁。测试寻寻觅觅咏梅.测试咏梅");
        article3.setCreate_time(new Date());

        List<Object> list = new ArrayList<>();
        list.add(article1);
        list.add(article2);
        list.add(article3);

        commodityServiceImpl.createMultiDoc(list);
    }

    @Test
    public void searchDoc(){
        int page =1;
        int size =10;
        System.out.println("==============================不指定索引，查询一个字段===============================================");
        SearchHit[] hits = commodityServiceImpl.searchDoc(null, "春晚", (page-1)*size, size, "title");
        for(SearchHit hit:hits){
            log.info("索引：{}，value:{}",hit.getIndex(),JSONObject.toJSONString(hit.getSourceAsString()));
        }
        System.out.println("\n\n==============================不指定索引，查询多个字段===============================================");
        hits = commodityServiceImpl.searchDoc(null, "春晚", (page-1)*size, size, "title","content");
        for(SearchHit hit:hits){
            log.info("索引：{}，value:{}",hit.getIndex(),JSONObject.toJSONString(hit.getSourceAsString()));
        }
        System.out.println("\n\n==============================指定索引，查询一个字段===============================================");
        hits = commodityServiceImpl.searchDoc(Article2.class, "春晚", (page-1)*size, size, "title");
        for(SearchHit hit:hits){
            log.info("索引：{}，value:{}",hit.getIndex(),JSONObject.toJSONString(hit.getSourceAsString()));
        }
        System.out.println("\n\n==============================指定索引，查询多个字段===============================================");
        hits = commodityServiceImpl.searchDoc(Article2.class, "春晚", (page-1)*size, size, "title","content");
        for(SearchHit hit:hits){
            log.info("索引：{}，value:{}",hit.getIndex(),JSONObject.toJSONString(hit.getSourceAsString()));
        }
    }





    /**
     * 更新
     */
    @Test
    public void updateDoc(){
        //先搜索一个文档
        int page =1;
        int size =1;
        System.out.println("===================只查询一条数据=========================");
        SearchHit[] hits = commodityServiceImpl.searchDoc(Article.class, "物是人非",(page-1)*size ,size, "content");
        String id;
        Article article;
        if(hits.length>=1){
            id = hits[0].getId();
            log.info("id:{}",id);
            log.info("搜索结果:{}", hits[0].getSourceAsString());
            article = JSONObject.parseObject(hits[0].getSourceAsString(), Article.class);
        }else{
            log.info("没有查询到数据");
            return;
        }
        article.setContent("更新测试_"+article.getContent());
        article.setTitle("更新测试_"+article.getTitle());
        System.out.println("===================更新数据=========================");
        Boolean updateResult = commodityServiceImpl.updateDoc(article, id);
        log.info("更新数据结果:{}",updateResult);

        //暂停五秒，等待数据更新之后再查询
        try {
            Thread.sleep(5*1000L);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println("===================使用id查询数据，确认是否更新成功=========================");
        SearchHit[] find_hits = commodityServiceImpl.findById(Article.class, id);
        log.info("更新操作后查询到的数据:{}",find_hits);

    }

    /**
     * 批量更新测试
     */
    @Test
    public void updateMultiDoc(){
        //先搜索一批文档
        int page =1;
        int size =10;
        System.out.println("===================查询数据=========================");
        SearchHit[] hits = commodityServiceImpl.searchDoc(Article.class, "物是人非",(page-1)*size ,size,"content");
        log.info("hits的数量:{}",hits.length);
        Article article;
        String id;
        ArrayList<HashMap<String, Object>> list = new ArrayList<>(16);
        if(hits.length>=1){
            int index = 1;
            for (SearchHit hit : hits) {
                HashMap<String, Object> map = new HashMap<>(4);
                id = hit.getId();
                article = JSONObject.parseObject(hit.getSourceAsString(), Article.class);
                article.setContent("批量更新测试_"+index+article.getContent());
                article.setTitle("批量更新测试_"+index+article.getTitle());
                map.put("id", id);
                map.put("obj", article);
                list.add(map);
                index++;
            }
        }else{
            log.info("没有查询到数据");
            return;
        }
        System.out.println("===================批量更新数据=========================");
        commodityServiceImpl.updateMultiDoc(list);
        //暂停五秒，等待数据更新之后再查询
        try {
            Thread.sleep(5*1000L);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("===================再次查询数据，确认批量更新是否成功=========================");
        SearchHit[] find_hits = commodityServiceImpl.searchDoc(Article.class, "物是人非",(page-1)*size ,size,"content");
        log.info("find_hits的数量:{}",find_hits.length);
        log.info("更新操作后查询到的数据:{}", JSONArray.toJSONString(find_hits));
    }


    @Test
    public void deleteDoc(){
        //先搜索一批文档
        int page =1;
        int size =1;
        System.out.println("===================先随便查询查询一个数据=========================");
        SearchHit[] hits = commodityServiceImpl.searchDoc(Article.class, "黄昏",(page-1)*size ,size,"content");
        log.info("hits的数量:{}",hits.length);
        ArrayList<String> list = new ArrayList<>(4);
        if(hits.length==0){
            log.info("没有查询到数据，先添加数据吧");
            return;
        }
        System.out.println("===================删除数据=========================");
        commodityServiceImpl.deleteById(Article.class.getSimpleName(), hits[0].getId());
        //暂停五秒
        try {
            Thread.sleep(5*1000L);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("===================再次查询数据，确认删除是否成功=========================");
        SearchHit[] find_hits = commodityServiceImpl.findById(Article.class, hits[0].getId());
        log.info("find_hits的数量:{}",find_hits.length);
    }

    @Test
    public void deleteMultiDoc(){
        //先搜索一批文档
        int page =1;
        int size =10;
        System.out.println("===================查询数据=========================");
        SearchHit[] hits = commodityServiceImpl.searchDoc(Article.class, "物是人非",(page-1)*size ,size,"content");
        log.info("hits的数量:{}",hits.length);
        ArrayList<String> list = new ArrayList<>(16);
        if(hits.length>=1){
            for (SearchHit hit : hits) {
                list.add(hit.getId());
            }
        }else{
            log.info("没有查询到数据，先添加数据");
            return;
        }
        System.out.println("===================批量删除数据=========================");
        commodityServiceImpl.deleteMultiDoc(Article.class.getSimpleName(), list);
        //暂停五秒，等待数据删除之后再查询
        try {
            Thread.sleep(5*1000L);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("===================再次查询数据，确认批量删除是否成功=========================");
        SearchHit[] find_hits = commodityServiceImpl.searchDoc(Article.class, "物是人非",(page-1)*size ,size,"content");
        log.info("find_hits的数量:{}",find_hits.length);
    }

}
